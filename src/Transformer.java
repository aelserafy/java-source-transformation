import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.io.FilenameUtils;

import nu.xom.Attribute;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Node;
import nu.xom.Nodes;
import nu.xom.ParsingException;
import nu.xom.Serializer;
import nu.xom.Text;
import nu.xom.ValidityException;
import nu.xom.XPathContext;

public class Transformer {
	static final String NEW_LINE = System.getProperty("line.separator");
	static String JAVA_PATH = "java -cp ." + java.lang.System.getProperty("path.separator");
	static String JAVA_C_PATH = "javac";
	static String SRC_ML = "srcml";

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		try {
			System.out.println("Java Source Transformation v.1.0.4");
			configureEnvironment();

			String javaClassFile = validateArgs(args);
			String javaFileExtension = FilenameUtils.getExtension(javaClassFile);
			String javaClassFileName = FilenameUtils.getBaseName(javaClassFile);
			String basePath = FilenameUtils.getFullPath(javaClassFile);

			boolean inputFileCompilesAndRuns = false;
			System.out.println("Verifying javac exists...");
			boolean javacRecognized = checkIfExecutesSuccessfully(JAVA_C_PATH, "-version", false);
			if (!javacRecognized) {
				System.err.println("Your system does not have a recognized \"javac\" command");
				System.err.println("You may adjust the config file");
				while (true) {
					System.out.println("Do you want to continue? (Y)es or (N)o");
					String answer = scanner.nextLine().toLowerCase();
					if (answer.equals("n") || answer.equals("no"))
						return;
					if (answer.equals("y") || answer.equals("yes"))
						break;
				}
			} else {
				System.out.println("Compiling input *.java file...");
				boolean classGenerated = checkIfExecutesSuccessfully(JAVA_C_PATH, javaClassFile, true);
				if (classGenerated) {
					System.out.println("Running input *.java file...");
					if (checkIfExecutesSuccessfully(JAVA_PATH + basePath, basePath + javaClassFileName, false))
						inputFileCompilesAndRuns = true;
				}
			}

			if (!inputFileCompilesAndRuns)
				System.err.println(
						"Please note that this application was not able to run the input *.java and consequently will not attempt running the transformed version");

			System.out.println("Verifying srcML exists...");
			boolean srcMLRecognized = checkIfExecutesSuccessfully(SRC_ML, "-h", false);
			if (!srcMLRecognized) {
				System.err.println("Unable to launch srcML");
				System.err.println("Please adjust the config file");
				System.err.println("Exiting transformation...");
				Runtime.getRuntime().exit(1);
			}

			System.out.println("Generating mapping *.xml file...");
			String javaClassXML = basePath + javaClassFileName + ".xml";
			boolean xmlGenerated = checkIfExecutesSuccessfully(SRC_ML, javaClassFile + " -o " + javaClassXML, true);
			if (!xmlGenerated) {
				System.err.println("Error when parsing source file: " + javaClassFile + " from srcML");
				Runtime.getRuntime().exit(2);
			}

			System.out.println("Transforming the *.xml file...");
			String newJavaClassFileName = javaClassFileName + "_transformed";
			Document doc = transformXMLDocument(javaClassFileName, newJavaClassFileName, javaClassXML);

			System.out.println("Writing the transformed *.xml file...");
			writeTransofrmedXML(javaClassXML, doc);

			System.out.println("Generating the transformed *.java file...");
			String newJavaClass = basePath + newJavaClassFileName + "." + javaFileExtension;
			boolean xmlParsed = checkIfExecutesSuccessfully(SRC_ML, javaClassXML + " -o " + newJavaClass, true);
			if (!xmlParsed)
				Runtime.getRuntime().exit(3);

			if (javacRecognized) {
				System.out.println("Compiling the transformed *.java file...");
				boolean javaGenerated = checkIfExecutesSuccessfully(JAVA_C_PATH, newJavaClass, true);
				if (!javaGenerated)
					Runtime.getRuntime().exit(4);

				if (inputFileCompilesAndRuns) {
					System.out.println("Running the transformed *.java file...");
					executeCommand(JAVA_PATH + basePath, basePath + FilenameUtils.getBaseName(newJavaClass), true);
				}
			}
		} catch (Exception ex) {
			handleException(ex);
		} finally {
			scanner.close();
		}
	}

	private static void configureEnvironment() throws FileNotFoundException, IOException {
		BufferedReader config = new BufferedReader(new FileReader("config"));
		try {
			System.out.println("Reading and setting paths for the executables...");
			String line = "";
			while ((line = config.readLine()) != null) {
				if (line.startsWith("#") || line.trim().isEmpty())
					continue;
				String[] nameAndValues = line.split("=");
				String name = nameAndValues[0].toLowerCase();
				if (name.equals("java"))
					JAVA_PATH = nameAndValues[1] + " -cp ." + java.lang.System.getProperty("path.separator");
				else if (name.equals("javac"))
					JAVA_C_PATH = nameAndValues[1];
				else if (name.equals("srcml"))
					SRC_ML = nameAndValues[1];
			}
		} catch (Exception ex) {
			handleException(ex);
		} finally {
			config.close();
		}
	}

	private static Document transformXMLDocument(String javaClassFileName, String newJavaClassFileName,
			String javaClassXML) throws ParsingException, ValidityException, IOException {
		Document doc = getXMLDocument(javaClassXML);
		XPathContext context = new XPathContext("x", "http://www.srcML.org/srcML/src");
		Nodes nodes = doc.query("//x:function//x:operator", context);

		int nodesSize = nodes.size();
		for (int i = 0; i < nodesSize; i++) {
			Node node = nodes.get(i);
			if (isAssignmnent(node)) {
				List<Node> sysoutNodes = getLHSNodes(node);
				if (!(sysoutNodes.size() > 0))
					sysoutNodes = getRHSNodes(node);
				Element sysoutStmt = getSysoutStmt(sysoutNodes);
				Element exprStmt = getNearestParentWithThisTageName(node, "expr_stmt");
				if (exprStmt == null)
					exprStmt = getNearestParentWithThisTageName(node, "decl_stmt");
				if (exprStmt != null)
					insertSysoutAfterThisExpr(exprStmt, sysoutStmt);
				expandParentBlockIfExists(node);
			}
		}

		nodes = doc.query("//x:function//x:init", context);
		nodesSize = nodes.size();
		for (int i = 0; i < nodesSize; i++) {
			Node node = nodes.get(i);
			List<Node> siblingNodesOfInterest = getSiblingNodesOfInterest(node);
			Element sysoutStmt = getSysoutStmt(siblingNodesOfInterest);
			Element exprStmt = getNearestParentWithThisTageName(node, "decl_stmt");
			if (exprStmt != null)
				insertSysoutAfterThisExpr(exprStmt, sysoutStmt);
			expandParentBlockIfExists(node);
		}

		Nodes mainClass = doc.query("//x:class/x:name[.='" + javaClassFileName + "']", context);
		if (mainClass.size() > 0) {
			Element className = (Element) mainClass.get(0);
			replaceNodeValue(className, newJavaClassFileName);
		}

		Nodes constructors = doc.query("//x:class//x:constructor/x:name[.='" + javaClassFileName + "']", context);
		int constructorsCount = constructors.size();
		for (int i = 0; i < constructorsCount; i++) {
			Element constructorName = (Element) constructors.get(i);
			replaceNodeValue(constructorName, newJavaClassFileName);
		}

		return doc;
	}

	private static List<Node> getLHSNodes(Node node) {
		List<Node> nodes = new ArrayList<Node>();
		Node parent = node.getParent();
		int childCount = parent.getChildCount();
		for (int i = 0; i < childCount; i++)
			if (considerChildAndBreakIfMatchingNode(parent.getChild(i), nodes, node))
				break;
		return nodes;
	}

	private static List<Node> getRHSNodes(Node node) {
		List<Node> nodes = new ArrayList<Node>();
		Node parent = node.getParent();
		int childCount = parent.getChildCount();
		for (int i = childCount - 1; i >= 0; i--)
			if (considerChildAndBreakIfMatchingNode(parent.getChild(i), nodes, node))
				break;
		return nodes;
	}

	private static boolean considerChildAndBreakIfMatchingNode(Node child, List<Node> nodes, Node node) {
		if (child != node)
			nodes.add(child);
		else
			return true;
		if (isAssignmnent(child))
			nodes.clear();
		return false;
	}

	private static boolean isAssignmnent(Node node) {
		String text = node.getValue().trim();
		return text.equals("=") || text.equals("+=") || text.equals("-=") || text.equals("*=") || text.equals("/=")
				|| text.equals("%=") || text.equals("&=") || text.equals("|=") || text.equals("^=")
				|| text.equals("<<=") || text.equals(">>=") || text.equals(">>>=") || text.equals("++")
				|| text.equals("--");
	}

	private static void expandParentBlockIfExists(Node node) {
		Element blockNode = getNearestParentWithThisTageName(node, "block");
		if (blockNode == null)
			return;
		Attribute attribute = blockNode.getAttribute("type");
		if (attribute == null)
			return;
		if (attribute.getValue().toLowerCase().equals("pseudo")) {
			blockNode.removeAttribute(attribute);
			blockNode.insertChild("{", 0);
			blockNode.insertChild(new Text(System.getProperty("line.separator")), 1);
			blockNode.insertChild(new Text(System.getProperty("line.separator")), blockNode.getChildCount());
			blockNode.insertChild("}", blockNode.getChildCount());
		}
	}

	private static void handleException(Exception ex) {
		System.err.println(ex.toString());
		ex.printStackTrace(System.err);
	}

	private static void writeTransofrmedXML(String javaClassXML, Document doc) throws IOException {
		FileOutputStream transformedXML = new FileOutputStream(javaClassXML);
		try {
			Serializer serializer = new Serializer(transformedXML);
			serializer.write(doc);
		} catch (Exception ex) {
			handleException(ex);
		} finally {
			transformedXML.close();
		}
	}

	private static List<Node> getSiblingNodesOfInterest(Node node) {
		Node declParent = node.getParent();
		List<Node> siblingNodesOfInterest = new ArrayList<Node>();
		int declParentChildrenCount = declParent.getChildCount();
		boolean startCollectingNodes = false;
		for (int j = 0; j < declParentChildrenCount; j++) {
			Node sibling = declParent.getChild(j);
			if (sibling == node)
				break;
			if (startCollectingNodes)
				siblingNodesOfInterest.add(sibling);
			else if (sibling instanceof Element) {
				Element siblingElement = (Element) sibling;
				if (siblingElement.getQualifiedName().equals("type"))
					startCollectingNodes = true;
			}
		}
		return siblingNodesOfInterest;
	}

	private static String validateArgs(String[] args) {
		if (args.length < 1) {
			System.err.println("This application takes a *.java file as an argument. Aborting transformation...");
			Runtime.getRuntime().exit(-1);
		}

		if (args[0].toLowerCase().equals("-h") || args[0].toLowerCase().equals("-help")) {
			System.out.println("The application takes 1 argument representing the *.java file to be transformed");
			Runtime.getRuntime().exit(0);
		}

		String javaClassFile = args[0];
		File file = new File(javaClassFile);
		String extension = FilenameUtils.getExtension(javaClassFile);
		if (!file.exists() || file.isDirectory() || !extension.toLowerCase().equals("java")) {
			System.err.println("Please use a valid *.java file path");
			Runtime.getRuntime().exit(-1);
		}
		return javaClassFile.replace("\\", "/");
	}

	private static void insertSysoutAfterThisExpr(Element exprStmt, Element sysoutStmt) {
		Element exprStmtParent = (Element) exprStmt.getParent();
		int exprStmtLocation = exprStmtParent.indexOf(exprStmt);
		exprStmtParent.insertChild(new Text(NEW_LINE), ++exprStmtLocation);
		exprStmtParent.insertChild(sysoutStmt, ++exprStmtLocation);
	}

	private static Element getNearestParentWithThisTageName(Node node, String tagName) {
		if (node instanceof Element)
			if (((Element) node).getQualifiedName().equals(tagName))
				return (Element) node;

		if (node.getParent() != null)
			return getNearestParentWithThisTageName(node.getParent(), tagName);
		else
			return null;
	}

	private static Element getSysoutStmt(List<Node> nodesToIncludeInSysout)
			throws ValidityException, ParsingException, IOException {
		String nodesText = getNodesText(nodesToIncludeInSysout);
		Document doc = getXMLDocument("templates/sysout.xml");
		XPathContext context = new XPathContext("x", "http://www.srcML.org/srcML/src");

		Element sysoutVarName = (Element) doc.query("//x:literal[.='##']", context).get(0);
		replaceNodeValue(sysoutVarName, "\"" + nodesText + "\"");

		Element sysoutExpr = (Element) doc.query("//x:expr", context).get(1);
		for (Node node : nodesToIncludeInSysout)
			sysoutExpr.appendChild(node.copy());

		return (Element) doc.query("//x:expr_stmt", context).get(0).copy();
	}

	private static Document getXMLDocument(String documentFile)
			throws ParsingException, ValidityException, IOException {
		Builder xmlParser = new Builder();
		File file = new File(documentFile);
		Document doc = xmlParser.build(file);
		return doc;
	}

	private static void replaceNodeValue(Element node, String newValue) {
		node.removeChildren();
		node.appendChild(newValue);
	}

	private static String getNodesText(List<Node> nodes) {
		StringBuilder text = new StringBuilder();
		for (Node node : nodes) {
			int childCount = node.getChildCount();
			if (childCount > 0) {
				List<Node> childNodes = new ArrayList<Node>();
				for (int i = 0; i < childCount; i++)
					childNodes.add(node.getChild(i));
				text.append(getNodesText(childNodes));
			} else
				text.append(node.getValue().trim());
		}
		return text.toString();
	}

	private static boolean checkIfExecutesSuccessfully(String cmd, String args, boolean showOutput)
			throws InterruptedException {
		if (executeCommand(cmd, args, showOutput) == 0)
			return true;
		return false;
	}

	private static int executeCommand(String cmd, String args, boolean showOutput) throws InterruptedException {
		try {
			Runtime rt = Runtime.getRuntime();
			String command = cmd.replace("\\", "/") + " " + args.replace("\\", "/");
			Process pr = rt.exec(command);

			consumeBuffer(pr.getInputStream(), showOutput, System.out);
			consumeBuffer(pr.getErrorStream(), showOutput, System.err);

			pr.waitFor();
			return pr.exitValue();
		} catch (IOException ex) {
			return -1;
		}
	}

	private static void consumeBuffer(InputStream buffer, boolean showOutput, PrintStream stream) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(buffer));
		String line = null;
		while ((line = in.readLine()) != null)
			if (showOutput)
				stream.println(line);
	}
}

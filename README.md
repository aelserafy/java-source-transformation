# Java Source Transformation v.1.0.4 #

This application transforms an input *.java file to an instrumented version that prints the values of variables after their assignations. It tests the input and output files for compilation and running.
### Supported operators: ###
* Simple assignment (=)
* Initialization statement
* Compound assignment (+=, -=, *=, /=, %=, &=, |=, ^=, <<=, >>=, >>>=)
* Post/Pre-increment/decrement operators (++, --)

## How to use? ##
* Please configure the paths in the "config" to fit your running system
* Run this application from terminal while passing the "to be transformed" *.java source file's path as an argument
* The transformed *.java will be generated in the same directory where the original file resides

**Example 1**: java-source-transformation.exe samples/FullClass.java

**Example 2**: java -jar java-source-transformation.jar samples/FullClass.java

**This release was tested on Windows 7 SP1 (32-bit, 64-bit) and Ubuntu 14.04 (64-bit)**


## Configurations: ##
* A file named "config" is placed in the same directory where the executable resides
* This file contains paths to "java", "javac" and "srcML" executables
* srcML v.0.9.5 beta binaries for Windows and Ubuntu 14.04 are included in the package

## Known limitations: ##
* This implementation does not support assignments inside control statements' conditional parenthesis part
* If a variable is assigned in a function call and the return value of this function is assigned to the same variable, two sysout statements are injected, but both capture the same value of the variable of interest

## Acknowledgments: ##
### This implementation was carried out with the help of these libraries/tools: ###
* srcML v.0.9.5 beta (http://www.srcml.org/)
* XOM v.1.2.10 (http://www.xom.nu/)
* Apache Commons IO v.2.4 (https://commons.apache.org/proper/commons-io/)
* launch4j v.3.8 (http://launch4j.sourceforge.net/)